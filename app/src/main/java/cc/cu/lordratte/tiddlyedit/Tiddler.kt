package cc.cu.lordratte.tiddlyedit

import java.io.Serializable

/**
 * Created by joshua on 05/11/2017.
 */
class Tiddler() : HashMap<String, String>(){

    fun getTitle() : String {
        return this["title"]?: ""
    }

    fun getText() : String {
        return this["text"]?: ""
    }

    fun setText(text: String) {
        this["text"] = text
    }

    constructor(serializable: HashMap<*, *>) : this() {
        update(serializable)
    }

    fun update(tiddler_data: HashMap<*, *>) {
        clear()
        for (ent in tiddler_data.entries) {
            if (ent.key is String && ent.value is String) {
                this[ent.key as String] = ent.value as String
            }
        }
    }

    fun getTags(): List<String> {
        val tags = this["tags"]?: ""
        val re = Regex("(?:^|[^\\S\\xA0])(?:\\[\\[(.*?)\\]\\])(?=[^\\S\\xA0]|\$)|([\\S\\xA0]+)")
        return re.findAll(tags, 0).map { it.value }.toList()
    }
}