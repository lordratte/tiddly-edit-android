package cc.cu.lordratte.tiddlyedit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*
import org.jsoup.Jsoup
import java.io.InputStream
import java.nio.charset.Charset

class MainActivity : AppCompatActivity(){
    val PREFS_FILENAME = "cc.cu.lordratte.tiddlyedit"
    val FILE_CHOOSE_REQUEST = 1
    var lv : ListView? = null

    companion object{
        val EDIT_REQUEST = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        loadWiki()
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    private fun loadWiki() {
        val prefs = this.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        var loc = prefs.getString("file_loc", null)
        val html : String

        var file : InputStream? = null
        try {
            file = contentResolver.openInputStream(Uri.parse(loc))
        } catch (e: Exception) {
            Log.e("loadWiki", e.toString())
            //TODO: Complain
            loc = null
        }
        if (loc == null) {
            file = resources.openRawResource(R.raw.default_wiki)
        }
        if (file == null) {
            return
        }

        html = file.readBytes().toString(Charset.defaultCharset())
        val tiddlers : HashMap<String, Tiddler>
        try {
            tiddlers = parse(html)
        } catch (e : Exception) {
            Toast.makeText(applicationContext, getString(R.string.read_file_failed), Toast.LENGTH_LONG).show()
            return
        }
        lv = findViewById<ListView>(R.id.tiddler_list)
        lv?.onItemClickListener = AdapterView.OnItemClickListener { _: AdapterView<*>, _: View, position: Int, _: Long ->
            (lv?.adapter as MainListAdapter).onClick(position)
        }
        lv?.adapter = MainListAdapter(this, tiddlers)
    }

    private fun parse(html: String) : HashMap<String, Tiddler>{
        val doc = Jsoup.parse(html)
        val sa = doc.getElementById("storeArea")
        val tiddlers : HashMap<String, Tiddler> = HashMap()
        for(t in sa.children()) {
            val tiddler = Tiddler()
            for (a in t.attributes()) {
                tiddler[a.key] = a.value
            }
            tiddler.setText(t.text())
            val title = tiddler.getTitle()
            if (title != null) {
                tiddlers[title] = tiddler
            }
        }
        return tiddlers

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    private fun doChooseFile(): Boolean {
        val intent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent()
                    .setAction(Intent.ACTION_OPEN_DOCUMENT)
                    .setType("*/*")
                    .addCategory(Intent.CATEGORY_OPENABLE)
        } else {
            Intent()
                    .setAction(Intent.ACTION_GET_CONTENT)
                    .setType("*/*")
        }
        startActivityForResult(Intent.createChooser(intent, "Select a TiddlyWiki file"), FILE_CHOOSE_REQUEST)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data ?: return
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == FILE_CHOOSE_REQUEST && resultCode == Activity.RESULT_OK) {
            val edit = this.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE).edit()
            Toast.makeText(applicationContext, data.data.toString(), Toast.LENGTH_LONG).show()
            edit.putString("file_loc", data.data.toString())
            edit.apply()
            loadWiki()
        } else if (requestCode == MainActivity.EDIT_REQUEST && resultCode == Activity.RESULT_OK) {
            Log.d("IF1", (data.extras.getString("title") != null).toString())
            Log.d("IF1__", data.extras.getString("title")?: "NULL")
            Log.d("IF2", (data.extras.getSerializable("tiddler") is HashMap<*,*>).toString())
            Log.d("IF2__", data.extras.getSerializable("tiddler")?.toString() ?: "NULL")
            for (key in data.extras.keySet()) {
                Log.d("KEY", key)
            }
            if (data.extras.getString("title") != null && data.extras.getSerializable("tiddler") is HashMap<*,*>) {
                (lv?.adapter as MainListAdapter).updateTiddler(data.extras.getString("title"), data.extras.getSerializable("tiddler") as HashMap<*,*>)
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            R.id.load_file -> doChooseFile()
            else -> super.onOptionsItemSelected(item)
        }
    }
}
