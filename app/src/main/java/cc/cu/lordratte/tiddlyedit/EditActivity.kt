package cc.cu.lordratte.tiddlyedit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText

import kotlinx.android.synthetic.main.activity_edit.*
import co.lujun.androidtagview.TagContainerLayout



class EditActivity : AppCompatActivity(), TextWatcher {
    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
    override fun afterTextChanged(edit: Editable?) {
        tiddler?.setText(edit.toString())
    }

    var tiddler: Tiddler? = null
    var titles: ArrayList<String>? = null
    var title: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            val intent = Intent()
            intent.putExtra("tiddler", tiddler)
            intent.putExtra("title", title)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val bundle = intent.extras

        if (bundle != null && bundle.getSerializable("tiddler") is HashMap<*, *>
                && bundle.getStringArrayList("titles") != null
                && bundle.getString("title") != null) {
            tiddler = Tiddler(bundle.getSerializable("tiddler") as HashMap<*, *>)
            titles = bundle.getStringArrayList("titles")
            title = bundle.getString("title")
            val tid = tiddler ?: return
            supportActionBar?.title = tid.getTitle()

            //Body
            val tiddler_body = findViewById<EditText>(R.id.tiddler_body)
            tiddler_body.setText(tid.getText())
            tiddler_body.addTextChangedListener(this)

            //Tags
            val mTagContainerLayout = findViewById<TagContainerLayout>(R.id.tags)
            mTagContainerLayout.tags = tid.getTags()
            Log.d("LENGTH", tid.getTags().size.toString())
            Log.d("TAGS", tid["tags"])

        }

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
    }

}
