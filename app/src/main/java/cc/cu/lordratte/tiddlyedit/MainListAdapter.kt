package cc.cu.lordratte.tiddlyedit

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.widget.TextView
import java.util.ArrayList

/**
 * Created by joshua on 05/11/2017.
 */
public class MainListAdapter(private val context: MainActivity, private val tiddlers: HashMap<String, Tiddler>) : BaseAdapter() {
    fun onClick(position: Int) {
        val edit_tiddler = Intent(context, EditActivity::class.java)
        val bundle = Bundle()
        val tids = getVisibleTiddlers()
        val title = tids.keys.sorted()[position]
        bundle.putSerializable("tiddler", tids[title])
        bundle.putStringArrayList("titles", ArrayList<String>(tids.keys))
        bundle.putString("title", title)
        edit_tiddler.putExtras(bundle)
        ActivityCompat.startActivityForResult(context, edit_tiddler, MainActivity.EDIT_REQUEST, bundle)
    }

    private val mInflator: LayoutInflater = LayoutInflater.from(context)
    var hide_system : Boolean = true

    private fun getVisibleTiddlers() : HashMap<String, Tiddler>{
        return tiddlers.filter { (!it.key.startsWith("$:") && hide_system) || !hide_system } as HashMap
    }

    override fun getCount(): Int {
        return getVisibleTiddlers().size
    }

    override fun getItem(position: Int): Any {
        val tids = getVisibleTiddlers()
        return tids[tids.keys.sorted()[position]] ?: return Tiddler()
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view: View?
        val tids = getVisibleTiddlers()
        val tiddler : Tiddler = tids[tids.keys.sorted()[position]] ?: Tiddler()

        if (convertView == null) {
            view = this.mInflator.inflate(R.layout.tiddler_entry, parent, false)
        } else {
            view = convertView
        }
        view?.findViewById<TextView>(R.id.tiddler_entry_title)?.text = tiddler.getTitle()
        return view
    }

    fun updateTiddler(title: String, tiddler_data: HashMap<*, *>) {
        val tid = tiddlers[title] ?: Tiddler()
        tid.update(tiddler_data)
        tiddlers.remove(title)
        tiddlers[tid.getTitle()] = tid
    }
}